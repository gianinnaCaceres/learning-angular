#Weather app for practicing Angular 4 & RxJs

Created this repo to practice Angular 4, TypeScript and RxJs by creating a weather app using free Open Weather API.

How to run the app:
```shell
npm install
npm start
```
##Icons used:

http://www.alessioatzeni.com/meteocons/

Inspired by Design from dribble: 

Open weather API used:

Current weather:
https://openweathermap.org/current

Forecast weather (TO-D0):
https://openweathermap.org/forecast5


![Alt text](/screenshots/day.png?raw=true "Day view")

![Alt text](/screenshots/night.png?raw=true "Night view")