export interface Coord {
    lon: number;
    lat: number;
}

export interface Weather {

    id: number;
    main: string;
    description: string;
    icon: string;

}

export interface Main {
    temp: number;
    pressure: number;
    humidity: number;
    temp_min: number;
    temp_max: number;
}
export interface Wind {
    speed: number;
    deg: number;
}

export interface Sys {
    type: number;
    id: number;
    message: number;
    country: string;
    sunrise: number;
    sunset: number;
}

export interface CurrentWeather {
    coord: Coord;
    weather: Array<Weather>;
    base: string;
    main: Main;
    visibility: number;
    wind: Wind;
    clouds: {
        all: number;
    }
    dt: number;
    sys: Sys;
    id: number;
    name: string;
    cod: number;
}

export interface ForecastMain extends Main {
    pressure: number;
    sea_level: number;
    grnd_level: number;
    humidity: number;
    temp_kf: number;
}
export interface ForecastList {
    dt: string;
    main: ForecastMain;
    weather: Weather[];
    wind: Wind;
    sys: { pod: string; }
    dt_txt: string;
}

export interface Forecast {
    city: {
        id: number;
        name: string;
    },
    coord: Coord;
    country: string;
    cod: string;
    message: number;
    cnt: number;
    list: Array<ForecastList>;
}