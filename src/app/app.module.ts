import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';


@NgModule({
  imports: [BrowserModule,
    ComponentsModule,
    FormsModule
    ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
