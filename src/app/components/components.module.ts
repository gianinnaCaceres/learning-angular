import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { WeatherCardComponent } from './weather-card.component/weather-card.component';
import { WeatherForecastLineComponent } from './weather-card.component/weather-forecast-line.component/weather-forecast-line.component';

import { WeatherDataService } from './weather-data.service';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        HttpModule,
        JsonpModule,
        BrowserModule,
        FormsModule
    ],
    declarations: [
        WeatherCardComponent,
        WeatherForecastLineComponent
    ],

    providers: [
        WeatherDataService
    ],

    exports: [
        WeatherCardComponent,
        WeatherForecastLineComponent
    ]

})
export class ComponentsModule { }

