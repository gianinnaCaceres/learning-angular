import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import * as model from '../../app.interfaces';
import { NgForm } from '@angular/forms';
import { WeatherDataService } from '../weather-data.service';
import { WeatherForecastLineComponent } from './weather-forecast-line.component/weather-forecast-line.component';

@Component({
  selector: 'weather-card',
  styleUrls: ['./weather-card.component.css'],
  templateUrl: './weather-card.component.html',
})
export class WeatherCardComponent {
  public city: string = '';
  public data: model.CurrentWeather;
  public icon = '';
  public weatherTime: string;
  public time = '';
  public forecastList: any;
  public units = 'metrics';
  public windSpeed: string;

  constructor(private weatherService: WeatherDataService) { }

  onSubmit(f: NgForm) {
    console.log(f.value);
    console.log(f.valid);

    this.weatherService
      .getWeatherByCity(f.value.city)
      .subscribe(data => {
        this.data = data;
        this.icon = this.weatherService.getIconByCode(this.data.weather[0].id, this.data.sys.sunrise, this.data.sys.sunset, this.data.dt);
        //this.weatherTime = moment(this.data.dt).format('MMMM Do YYYY, h:mm a');
        if (this.data.dt >= this.data.sys.sunrise && this.data.dt < this.data.sys.sunset) {
          this.time = 'day'
        }
        else {
          this.time = 'night';
        }
        this.windSpeed = (this.data.wind.speed * 3.6).toFixed(2); //convert it to kmh
      });

    this.weatherService
      .getForecast(f.value.city)
      .subscribe(forecast => {
        this.forecastList = forecast;
        console.log(forecast);
      });
  }

}
