import { Component, OnInit, Input } from '@angular/core';
import * as model from '../../../app.interfaces';
import { WeatherDataService } from '../../weather-data.service';

@Component({
    selector: 'weather-forecast-line',
    styleUrls: ['./weather-forecast-line.component.css'],
    templateUrl: './weather-forecast-line.component.html',
})
export class WeatherForecastLineComponent implements OnInit {
    @Input() tempMax: number;
    @Input() tempMin: number;
    @Input() day: string;
    @Input() code: number;
    public icon = '';

    constructor(private weatherService: WeatherDataService) { }
    ngOnInit() {
        this.icon = this.weatherService.getIconByCode(this.code);
    }

}
