// Imports
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import * as model from '../app.interfaces';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WeatherDataService {

    constructor(private http: Http) { }
    private baseURL = 'http://api.openweathermap.org/data/2.5/';
    private appId = '4b24bdd5ab41bd41b9074ec5bb4026f9';
    private units = 'metric';
    private mockup_data = { "coord": { "lon": -92.5, "lat": 38.25 }, "weather": [{ "id": 800, "main": "Clear", "description": "clear sky", "icon": "01d" }], "base": "stations", "main": { "temp": 23.66, "pressure": 1022, "humidity": 36, "temp_min": 23, "temp_max": 25 }, "visibility": 16093, "wind": { "speed": 2.1, "deg": 320 }, "clouds": { "all": 1 }, "dt": 1498348680, "sys": { "type": 1, "id": 1645, "message": 0.0029, "country": "US", "sunrise": 1498387685, "sunset": 1498441041 }, "id": 4398678, "name": "Missouri", "cod": 200 };
   
    //Get weather data by city name
    //api.openweathermap.org/data/2.5/weather?q={city name}
    // api.openweathermap.org/data/2.5/weather?q={city name},{country code}
    getWeatherByCity(city: string): Observable<model.CurrentWeather> {
        //...using get request
        return this.http.get(this.baseURL + 'weather?q=' + city + '&appId=' + this.appId + '&units=' + this.units)
            // ...and calling .json() on the response to return data
            .map((res: Response) => res.json())
            //...errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
        // return Observable.of(this.mockup_data);
    }

    //Get icons by code
    getIconByCode(code: number, sunrise?: number, sunset?: number, currentTime?: number): string {
        const range = code / 100;  //get the range
        let icon = '';

        // 800 = clear sky, distinct icon from day - night
        if (code === 800) { //clear sky
            if (currentTime && sunrise && sunset) {
                if (currentTime >= sunrise && currentTime < sunset) {
                    icon = 'B'; //sun 
                }
                else {
                    icon = 'C';  //moon
                }
            } else {
                icon = 'B'; //sun 
            }
        } else if (code >= 801 && code <= 804) {
            if (currentTime && sunrise && sunset) {
                if (currentTime >= sunrise && currentTime < sunset) {
                    icon = 'H'; //cloud with sun 
                }
                else {
                    icon = 'I';  //cloud with moon
                }
            } else {
                icon = 'B'; //sun 
            }
        } else if (code === 905) {
            icon = 'F'; //windy
        } else if (code === 904) {
            icon = "'"; //hot
        } else {
            switch (Math.floor(range)) { //take the int only
                case 2:
                    icon = 'P'; //thunderstorm
                    break;
                case 3:
                    icon = 'Q'; //drizzle
                    break;
                case 5:
                    icon = 'R'; // rainy
                    break;
                case 6:
                    icon = 'X';
                    break;
                case 7:
                    icon = 'M'; // foggy
                    break;
                case 8:
                    icon = 'B'; //sun 
                    break;

            }

        }
        return icon;
    }


// Gets 5 days forecast
// api.openweathermap.org/data/2.5/forecast?q={city name},{country code}
// TO-DO: 
// Define interface for forecast data
// Filter data to get max and min temperature per day and set icon accordingly
// Filtered data should be like this:
// { dt: 'Sunday' , temp_max: 34, temp_min: 23, id: 800 }

getForecast(city: string): Observable < any > {
    // return this.http.get(this.baseURL + 'forecast?q=' +  city + '&appId='+ this.appId + '&units=' + this.units)
    //     // ...and calling .json() on the response to return data
    //     .map((res: Response) => res.json())
    //     //...errors if any
    //     .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return Observable.of(
        [{
            day: 'Sunday',
            temp_max: 34,
            temp_min: 23,
            id: 800
        }, {
            day: 'Monday',
            temp_max: 30,
            temp_min: 20,
            id: 502
        },
        {
            day: 'Tuesday',
            temp_max: 37,
            temp_min: 27,
            id: 801
        }]
    )
}



    }

